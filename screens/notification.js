import React from "react";
import { Text, View, SafeAreaView, StyleSheet } from "react-native";

const Notification = (props) => {
    return (
        <SafeAreaView>
            <View style={styles.custom_notification}>
                <Text>Try Notification me! 🎉</Text>
            </View>
        </SafeAreaView>
    );
};
export default Notification;

const styles = StyleSheet.create({
    custom_notification: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
});
