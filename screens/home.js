import React from "react";
import { Text, View, SafeAreaView, ScrollView, StyleSheet } from "react-native";
import AddPost from "../components/AddPost";
import AllPosts from "../components/AllPosts";
import TopNav from "../components/TopNav";

const Home = (props) => {
    return (
        <SafeAreaView>
            <ScrollView style={styles.custom_home}>
                <TopNav />
                <AddPost />
                <AllPosts />
                <Text>Home</Text>
            </ScrollView>
        </SafeAreaView>
    );
};
export default Home;
const styles = StyleSheet.create({
    custom_home: {
        width: "100%",
        height: "100%",
    },
});
