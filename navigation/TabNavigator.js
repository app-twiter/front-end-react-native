import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { Ionicons } from "@expo/vector-icons";
import {
  HomeNavigator,
  NotificationNavigator,
  SettingNavigator,
} from "./StackNavigator";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          if (route.name === "Trang chủ") {
            return (
              <Ionicons
                name={"ios-home-outline"}
                size={focused ? 25 : 20}
                color={focused ? "#3366FF" : "black"}
              />
            );
          } else if (route.name === "Thông báo") {
            return (
              <Ionicons
                name={"notifications-outline"}
                size={focused ? 25 : 20}
                color={focused ? "#3366FF" : "black"}
              />
            );
          } else if (route.name === "Cài đặt") {
            return (
              <Ionicons
                name={"settings-outline"}
                size={focused ? 25 : 20}
                color={focused ? "#3366FF" : "black"}
              />
            );
          }
        },
      })}
      tabBarOptions={{
        activeTintColor: "#3366FF",
        labelStyle: {
          fontWeight: "500",
          fontSize: 11,
        },
      }}
    >
      <Tab.Screen name="Trang chủ" component={HomeNavigator} />
      <Tab.Screen name="Thông báo" component={NotificationNavigator} />
      <Tab.Screen name="Cài đặt" component={SettingNavigator} />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
