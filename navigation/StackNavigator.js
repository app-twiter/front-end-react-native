import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Setting from "../screens/Setting";
import Home from "../screens/Home";
import Notification from "../screens/Notification";

const Stack = createStackNavigator();

const customHeader = {
    headerTintColor: "white",
    headerStyle: { backgroundColor: "#fff" },
    headerTintColor: "blue",
    headerTitleStyle: {
        fontWeight: "bold",
        fontSize: 30,
    },
    headerTitleAlign: "left",
    headerStyle: {
        borderBottomWidth: 0.5,
    },
};

const HomeNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{ headerShown: false }}
                // options={{
                //   headerTintColor: "white",
                //   headerStyle: { backgroundColor: "#fff", borderBottomWidth: 0.5 },
                //   headerTintColor: "blue",
                //   title: "Twiter",
                //   headerTitleStyle: {
                //     fontWeight: "bold",
                //     fontSize: 35,
                //   },
                //   headerTitleAlign: "left",
                // }}
                name="Trang chủ"
                component={Home}
            />
        </Stack.Navigator>
    );
};

const NotificationNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{ headerShown: false }}
                name="Thông báo"
                component={Notification}
            />
        </Stack.Navigator>
    );
};

const SettingNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                options={{ headerShown: false }}
                name="Cài đặt"
                component={Setting}
            />
        </Stack.Navigator>
    );
};

export { HomeNavigator, NotificationNavigator, SettingNavigator };
