import { Ionicons } from "@expo/vector-icons";
import * as React from "react";
import { Image, StyleSheet, View, TouchableOpacity, Text } from "react-native";

export default function TopNav(props) {
  const { onPressSearch, onPressNotification } = props;
  return (
    <View style={styles.header_nav}>
      <Text style={styles.custom_title}>thong bao</Text>
      <View style={styles.action_left}>
        <TouchableOpacity>
          <Ionicons
            style={styles.icon_notification}
            size={24}
            name="ios-notifications-outline"
            onPress={onPressNotification}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Ionicons
            style={styles.icon_search}
            size={24}
            name="ios-search-outline"
            onPress={onPressSearch}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header_nav: {
    display: "flex",
    maxWidth: "100%",
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomWidth: 1,
    borderBottomColor: "#dddddd63",
  },
  custom_title: {
    left: 15,
    fontSize: 35,
    fontWeight: "bold",
    color: "#3366FF",
  },
  action_left: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    width: 150,
    maxHeight: "100%",
  },
  icon_notification: {},
  icon_search: {},
});
