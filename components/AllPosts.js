import { Ionicons } from "@expo/vector-icons";
import * as React from "react";
import { Image, StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { useFonts } from "expo-font";

export default function AllPosts(props) {
  const { onPressSearch, onPressNotification } = props;
  const [loaded] = useFonts({
    Montserrat_Regular: require("../assets/fonts/Montserrat-Regular.ttf"),
    Montserrat: require("../assets/fonts/Montserrat-Medium.ttf"),
  });
  if (!loaded) {
    return null;
  }
  return (
    <View style={styles.all_post}>
      <View style={styles.header_nav}>
        <Image
          style={styles.custom_image}
          source={require("../assets/image_user.png")}
        />
        <View style={styles.add_post}>
          <View style={styles.custom_infor}>
            <TouchableOpacity>
              <Text style={styles.custom_text_post}>Phan tấn tĩnh</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.custom_status_text_post}>
                đã cập nhật trạng thái của anh ấy
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.custom_option}>
            <Text style={styles.custom_time_post}>16 phút trước</Text>
            <View style={styles.custom_option}>
              <Ionicons name="create-outline" />
              <Ionicons name="trash-outline" />
            </View>
          </View>
        </View>
      </View>
      <View>
        <Text>Buồn quá ahihi</Text>
        <Image
          style={styles.custom_image_post}
          source={require("../assets/image_name.png")}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  all_post: {
    top: 35,
    height: "100%",
    width: "100%",
  },
  header_nav: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
  },
  custom_image: {
    width: "20%",
    height: 45,
  },
  add_post: {
    display: "flex",
    flexDirection: "column",
    width: "80%",
  },
  custom_option: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  custom_infor: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  custom_text_post: {
    fontFamily: "Montserrat",
  },
  custom_status_text_post: {
    fontFamily: "Montserrat_Regular",
  },
  custom_time_post: {
    fontFamily: "Montserrat_Regular",
    left: 5,
    fontSize: 13,
    top: 5,
  },
  custom_image_post: {
    width: "100%",
    height: "100%",
  },
});
