import { Ionicons } from "@expo/vector-icons";
import * as React from "react";
import { Image, StyleSheet, View, TouchableOpacity, Text } from "react-native";
import { useFonts } from "expo-font";

export default function AddPost(props) {
  const { onPressSearch, onPressNotification } = props;
  const [loaded] = useFonts({
    Montserrat: require("../assets/fonts/Montserrat-Regular.ttf"),
  });
  if (!loaded) {
    return null;
  }
  return (
    <View style={styles.header_nav}>
      <Image
        style={styles.custom_image}
        source={require("../assets/image_user.png")}
      />
      <View style={styles.add_post}>
        <TouchableOpacity>
          <Text style={styles.custom_text_post}>What's on your mind?</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header_nav: {
    display: "flex",
    maxWidth: "100%",
    height: 70,
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 1,
    borderBottomColor: "#dddddd8c",
  },
  custom_image: {
    width: "20%",
    height: 45,
  },
  add_post: {
    width: "80%",
  },
  custom_text_post: {
    fontFamily: "Montserrat",
  },
  icon_notification: {},
  icon_search: {},
});
